import { useState } from "react";
import TextField from "@mui/material/TextField";
import { NumericFormat } from "react-number-format";

function SalaryInput({handleBaseSalaryChange}) {
    const [baseSalary, setBaseSalary] = useState('');
    return (
        <NumericFormat
            customInput={ TextField }
            label='Gaji Bulanan'
            variant="outlined"
            valueIsNumericString={true}
            thousandSeparator={true}
            allowNegative={false}
            decimalScale={0}
            value={baseSalary}
            onValueChange={(value, sourceInfo) => {
                setBaseSalary(value.value);
                handleBaseSalaryChange(value.value);
            }}
            InputProps={{
                startAdornment: <span>Rp</span>
            }}
        />
    );
}

export default SalaryInput;

import {useState} from "react";
import {DatePicker, LocalizationProvider, TimePicker} from '@mui/x-date-pickers';
import {AdapterDayjs} from '@mui/x-date-pickers/AdapterDayjs';
import 'dayjs/locale/id';


function Entry({propId, propDate, propStart, propFinish, handleEntryChange}) {
    const [date, setDate] = useState(propDate);
    const [start, setStart] = useState(propStart);
    const [finish, setFinish] = useState(propFinish);

    return (
        <LocalizationProvider dateAdapter={AdapterDayjs} adapterLocale='id'>
            <div style={{display: 'flex', gap: '5px'}}>
                <DatePicker label="Tanggal Lembur"
                            value={date}
                            onAccept={(newDate) => {
                                setDate(newDate);
                                handleEntryChange({id: propId, date: newDate}, 'date')
                            }}
                />
                <TimePicker label="Mulai"
                            ampm={false}
                            value={start}
                            // maxTime={finish}
                            onAccept={(newStart) => {
                                setStart(newStart);
                                handleEntryChange({id: propId, start: newStart}, 'start')
                            }}
                />
                <TimePicker label="Selesai"
                            ampm={false}
                            value={finish}
                            // minTime={start}
                            onAccept={(newFinish) => {
                                setFinish(newFinish);
                                handleEntryChange({id: propId, finish: newFinish}, 'finish')
                            }}
                />
            </div>
        </LocalizationProvider>
    )
}

export default Entry;
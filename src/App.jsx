import {useEffect, useState} from "react";
import Entry from "./Entry.jsx";

import {AppBar, Button, Container, Grid, Paper, Typography} from "@mui/material";
import dayjs from "dayjs";
import SalaryInput from "./SalaryInput.jsx";
import Result from "./Result.jsx";
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import Icon from '@mdi/react';
import { mdiGitlab } from '@mdi/js';


function App() {
    const [baseSalary, setBaseSalary] = useState(0);
    const initialTime = dayjs().set('hour', 17).set('minute', 30);
    const [listEntry, setListEntry] = useState(
        [
            {id: 0, date: dayjs(), start: initialTime, finish: initialTime},
        ]
    );
    const [removeDisabled, setRemoveDisabled] = useState(false);
    const [elevatedId, setElevatedId] = useState(null);

    useEffect(() => {
        setRemoveDisabled(listEntry.length === 1);
    }, [listEntry]);

    const getLastId = () => {
        const lastEntry = listEntry[listEntry.length - 1];
        return lastEntry.id;
    }

    const handleEntryChange = (entry, updatedAttr) => {
        const updatedEntryList = listEntry.map((entryOnList) => {
            if (entryOnList.id === entry.id) {
                return {...entryOnList, [updatedAttr]: entry[updatedAttr]}
            }
            return entryOnList;
        });
        setListEntry(updatedEntryList);
    }

    const handleBaseSalaryChange = (baseSalaryInput) => {
        setBaseSalary(baseSalaryInput);
    }

    const addToList = () => {
        const newEntry = {
            id: getLastId() + 1,
            date: dayjs(),
            start: initialTime,
            finish: initialTime
        }
        setListEntry((currentList) => [...currentList, newEntry]);
    }

    const removeFromList = (id) => {
        setListEntry((currentList) => {
            return currentList.filter((en) => en.id !== id);
        });
    }

    // raimu kurang gawean
    return (
        <>
            <Container>
                <AppBar position="sticky" sx={{padding: '10px 0'}}>
                    <Typography
                        variant='h4'
                        sx={{
                            ml: 5,
                            display: { xs: 'none', md: 'flex' },
                            letterSpacing: '.3rem',
                            color: 'inherit',
                            textDecoration: 'none',
                        }}
                    >Overtime Calculator</Typography>
                </AppBar>
                <Typography variant='overline' display='block' align='center'>🤑🤑 Sudahkah anda lembur hari ini?
                    🤑🤑</Typography>
                <Paper id="entry-paper" sx={{margin: '10px 0', padding: '20px'}}
                       elevation={elevatedId === "entry-paper" ? 5 : 2} // gak penting
                       onMouseEnter={() => setElevatedId("entry-paper")}
                       onMouseLeave={() => setElevatedId(null) }
                >
                    <ul>
                        <li style={{listStyle: 'none', margin: '20px 0'}}>
                            <SalaryInput handleBaseSalaryChange={handleBaseSalaryChange}/>
                        </li>
                        {listEntry.map((entry) => {
                            return (
                                <div key={entry.id} style={{display: 'flex', gap: '10px', marginBottom: '10px'}}>
                                    <li style={{listStyle: 'none'}} key={entry.id}>
                                        <Entry
                                            propId={entry.id}
                                            propDate={entry.date}
                                            propStart={entry.start}
                                            propFinish={entry.finish}
                                            handleEntryChange={handleEntryChange}
                                        />
                                    </li>
                                    <Button
                                        disabled={removeDisabled}
                                        variant="outlined"
                                        onClick={() => removeFromList(entry.id)}
                                    ><RemoveCircleIcon /></Button>
                                    {
                                        entry.id === getLastId() ?
                                            <Button
                                                variant="outlined"
                                                onClick={addToList}
                                            ><AddCircleIcon /></Button>
                                            : null
                                    }
                                </div>
                            )
                        })}
                    </ul>
                </Paper>
                <Grid container columnSpacing='5px' sx={{mb: '20px'}}>
                    <Grid item xs={6}>
                        <Paper id="result-paper" sx={{padding: '10px', minHeight: '100%'}}
                               elevation={elevatedId === "result-paper" ? 5 : 2}
                               onMouseEnter={() => setElevatedId("result-paper")}
                               onMouseLeave={() => setElevatedId(null) }
                        >
                            <Result listEntry={listEntry} baseSalary={baseSalary}/>
                        </Paper>
                    </Grid>
                    <Grid item xs={6}>
                        <Paper id="about-paper" sx={{padding: '10px', minHeight: '100%'}}
                               elevation={elevatedId === "about-paper" ? 5 : 2}
                               onMouseEnter={() => setElevatedId("about-paper")}
                               onMouseLeave={() => setElevatedId(null) }
                        >
                            <ul>
                                <li><Typography>Disclaimer: keakuratan tidak terjamin</Typography></li>
                                <li><Typography>Sudah termasuk perhitungan lembur akhir pekan</Typography></li>
                                <li><Typography><s>Belum termasuk libur nasional</s> Libur nasional belum lintas tahun (todo)</Typography></li>
                                <li><Typography>Belum termasuk versi 6 hari kerja (todo)</Typography></li>
                                <li><Typography>UI belum responsive (todo)</Typography></li>
                            </ul>
                            <div style={{textAlign: 'center'}}>
                                <a href='https://gitlab.com/rosyidharyadi/overreact' target='_blank'>
                                    <Icon path={mdiGitlab}
                                          title="Repository"
                                          size={2}
                                          color="blue"
                                          spin
                                    />
                                </a>
                            </div>
                        </Paper>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default App
